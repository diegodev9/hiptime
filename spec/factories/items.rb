# == Schema Information
#
# Table name: items
#
#  id           :bigint           not null, primary key
#  completed_at :datetime
#  description  :text
#  title        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :bigint
#
# Indexes
#
#  index_items_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :item do
    title { "MyString" }
    description { "MyText" }
  end
end
