# frozen_string_literal: true

class ItemsController < ApplicationController
  before_action :check_user_signed_in
  before_action :set_item, only: %i[show edit update destroy]

  def index
    @items = Item.where(user_id: current_user.id).order(created_at: :desc)
  end

  def new
    @item = current_user.items.build
  end

  def show; end

  def create
    @item = current_user.items.build(item_params)
    if @item.save
      redirect_to root_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @item.update(item_params)
      redirect_to item_path(@item)
    else
      render :edit
    end
  end

  def destroy
    @item.destroy
    redirect_to root_path
  end

  def complete
    @item = Item.find(params[:id])
    @item.update(completed_at: Time.zone.now)
    redirect_to root_path
  end

  private

  def check_user_signed_in
    redirect_to welcomepage_path unless user_signed_in?
  end

  def set_item
    @item = Item.find(params[:id])
  end

  def item_params
    params.require(:item).permit(:title, :description)
  end
end
