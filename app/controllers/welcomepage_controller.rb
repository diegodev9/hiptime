# frozen_string_literal: true

class WelcomepageController < ApplicationController
  def index; end
end
